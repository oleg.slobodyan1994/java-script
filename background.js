const button = document.getElementById("btn");
const color = document.querySelector(" .color");

const colors = [
    "#F1f5f8",
    "Green",
    "Rgba(133, 122, 200)",
    "Red",
    "#F15025",
];

button.addEventListener("click", () => {
    let hexColor = colors[getRandomNumber()];
    document.body.style.backgroundColor = hexColor;
    color.textContent = hexColor;
})

function getRandomNumber() {
    return Math.floor(Math.random() * colors.length);
}